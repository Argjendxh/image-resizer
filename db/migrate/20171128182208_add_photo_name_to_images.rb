class AddPhotoNameToImages < ActiveRecord::Migration[5.0]
  def change
    add_column :images, :photo_name, :string
  end
end
