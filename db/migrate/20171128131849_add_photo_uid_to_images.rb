class AddPhotoUidToImages < ActiveRecord::Migration[5.0]
  def change
    add_column :images, :photo_uid, :string
  end
end
