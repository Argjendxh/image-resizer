class Image < ApplicationRecord

  	dragonfly_accessor :photo 

	before_save  :shorten_url
	

	def shorten_url
	   chars = [0..9, "A".."Z", 'a'..'z'].map { |range| range.to_a }.flatten
	   self.url =  6.times.map { chars.sample }.join
	end
end

