# Image Resizer

Image Resizer is a web-app where a user can resize any image from internet, by submitting
to the form that image's url and the desired max height and width. 
After submitting the form, the resized image is returned to the user.

# Ruby on Rails:

This app was developed using Ruby 2.4 and Rails 5.0.6
